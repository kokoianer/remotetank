# A lot of information about object oriented programming is from here:
# https://www.youtube.com/watch?v=JeznW_7DlB0
# and for Sockets it is from here:
# https://www.youtube.com/watch?v=Lbfe3-v7yE0

import socket
import time
import threading
import pickle

class SocketCom:
    def __init__(self, serverIP, serverPort, sendDict, recDict, sendInterval, timeout, debug = False):
        self.Headersize = 10
        self.serverIP = serverIP
        self.serverPort = serverPort
        self.sendDict = sendDict
        self.recDict = recDict
        self.sendInterval = sendInterval
        self.timeout = timeout
        self.upCounterSender = 0
        self.upCounterReceiver = 0
        self.msg_len = 0
        self.debug = debug
        self.conState = 0
        self.serverSoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.clientSoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.socketComTimeoutThread = threading.Thread(target = self.timeoutChecker,args=[])
        self.socketComTimeoutThread.daemon = True
        self.socketComTimeoutThread.start()

    def startServer(self):
        self.socketComServerThread = threading.Thread(target = self.runServer,args=[])
        self.socketComServerThread.daemon = True
        self.socketComServerThread.start()

    def runServer(self):
        self.serverSoc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.serverSoc.bind((self.serverIP, self.serverPort))
        print("Server started")
        self.serverSoc.listen(5)
        while True:
            print("waiting on new Connection")
            self.clientSoc, self.address = self.serverSoc.accept()
            self.clientSoc.settimeout(2.0)
            print("connected as server")
            self.conState = 1
            self.socketComReceiverThread = threading.Thread(target = self.conReceiver,args=[])
            self.socketComReceiverThread.daemon = True
            self.socketComReceiverThread.start()
            self.conSender()
                
    def startClient(self):
        self.socketComClientThread = threading.Thread(target = self.runClient,args=[])
        self.socketComClientThread.daemon = True
        self.socketComClientThread.start()

    def runClient(self):
        print("client Started")
        while True:
            time.sleep(self.sendInterval*10)
            for key in self.recDict:
                self.recDict[key]=0
            try:
                self.clientSoc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.clientSoc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.clientSoc.connect((self.serverIP, self.serverPort))
                self.clientSoc.settimeout(2.0)
                self.conState=1
                print("connected as client")
            except Exception as e:
                self.conState = 0
                if(self.debug):
                    print("Error in runClient, while connecting: ",e)
                time.sleep(0.1)
            self.socketComThread = threading.Thread(target = self.conReceiver,args=[])
            self.socketComThread.daemon = True
            self.socketComThread.start()
            self.conSender()


    def conReceiver(self):
        unpackErrorCounter = 0
        while(self.conState == 1):
            self.msg = b""
            try:
                self.msg =self.clientSoc.recv(1024)
            except Exception as e:
                if(self.debug):
                    print("Error while Receiving: ", e)
                self.conState = 0
                self.clientSoc.close()
                print("connection lost while receiving")
            try:
                self.msg_len = int(self.msg[:self.Headersize])
                tempRec = pickle.loads(self.msg[self.Headersize:])   
                unpackErrorCounter = 0
            except Exception as e:
                unpackErrorCounter += 1
                if(self.debug):
                    print("Error while unpack: ",e, "ErrorCounter: ", unpackErrorCounter)
                if(unpackErrorCounter == 100):
                    self.conState = 0
                    self.clientSoc.close()
                    print("connection lost while unpacking")

            if self.msg_len == (len(self.msg)-self.Headersize):
                try: 
                    self.upCounterReceiver = tempRec["upCounter"]
                    del tempRec["upCounter"]
                except:
                    if(self.debug):
                        print("Timout counter not sent")
                for key in tempRec:
                    self.recDict[key] = tempRec[key]

    def conSender(self):
        time.sleep(1)
        tempSend = dict()
        while (self.conState == 1):
            time.sleep(self.sendInterval)
            self.upCounterSender += 1
            for key in self.sendDict:
                tempSend[key] = self.sendDict[key]
            tempSend["upCounter"] = self.upCounterSender
            if(self.upCounterSender >= 8000):
                self.upCounterSender = 0
            self.msg = pickle.dumps(tempSend)
            self.msg=bytes(f"{len(self.msg):<{self.Headersize}}", 'utf-8')+self.msg
            try:
                self.clientSoc.send(self.msg)
            except Exception as e:
                if(self.debug):
                    print("lost sender Connection: ", e)

                #self.conState = 0
    
    def timeoutChecker(self):
        print("timoutChecker Started")
        self.oldUpCounterValue = 0
        self.timeoutCounter = 0

        #Fore testing purpouse only
        self.maxTimeout = 0
        self.oneMinCounter = 0

        while True:
            time.sleep(0.01)
            self.timeoutCounter += 1

            #For testing purpose only
            if(self.debug):
                self.oneMinCounter +=1
                if(self.oneMinCounter >= 1000):
                    
                    self.oneMinCounter = 0
                    print(f"the max time between two messages was {self.maxTimeout*10} ms, max counter cleared")
                    self.maxTimeout = 0

            if(self.timeoutCounter>self.maxTimeout):
                self.maxTimeout = self.timeoutCounter 
            #print(self.timeoutCounter)
            if(self.timeoutCounter >= self.timeout*100):
                for key in self.recDict:
                    self.recDict[key] = 0
                #print("timeout Reached")
            if(self.oldUpCounterValue != self.upCounterReceiver):
                self.timeoutCounter = 0
                self.oldUpCounterValue = self.upCounterReceiver


def main():
    print("main should not be called")



if __name__ == "__main__":
    main()