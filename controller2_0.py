import can2_0 as can
import time
import threading


class Controller:
    def __init__(self,maxValue = 0.9, debug = False):
        self.maxValue = maxValue
        self.debug = debug
        self.steer = 0
        self.throttle = 0
        self.canConnection=can.CanCom()
        self.canConnection.connect()


    def start(self, inputDict, debug = False):
        self.debug = debug
        self.inputDict = inputDict
        self.ControllerThread = threading.Thread(target = self.cruiseControl,args=[])
        self.ControllerThread.daemon = True
        self.ControllerThread.start()

        
    def cruiseControl(self):
        while True:
            if(self.inputDict["RB"]==0):
                self.steer=0
                self.throttle=0
            else:
                self.steer=self.inputDict["RJ_RL"]*self.maxValue
                self.throttle=self.inputDict["RJ_UD"]*self.maxValue
            self.leftMotor=self.steer
            self.rightMotor=-self.steer
            self.marigin=self.maxValue-abs(self.leftMotor)
            self.rmarigin=self.maxValue-abs(self.rightMotor)
            if(self.rmarigin<self.marigin):
                self.marigin=self.rmarigin
            if((self.throttle>self.marigin)or(self.throttle<-self.marigin)):
                if(self.throttle>0):
                    self.leftMotor=self.leftMotor+self.marigin
                    self.rightMotor=self.rightMotor+self.marigin
                else:
                    self.leftMotor=self.leftMotor-self.marigin
                    self.rightMotor=self.rightMotor-self.marigin
            else:
                self.leftMotor=self.leftMotor+self.throttle
                self.rightMotor=self.rightMotor+self.throttle
            if(self.debug):
                print("marigin"+str(self.marigin))
                print("left corrected: "+str(self.leftMotor))
                print("right corrected: "+str(self.rightMotor))
            if(self.leftMotor<0):
                temp=int(self.leftMotor*-128+128)
                self.canConnection.cansend(0x11,[temp])
                #print("cansend")
            else:
                temp=int(self.leftMotor*128)
                self.canConnection.cansend(0x11,[temp])
            if(self.rightMotor<0):
                temp=int(self.rightMotor*-128+128)
                self.canConnection.cansend(0x21,[temp])
            else:
                temp=int(self.rightMotor*128)
                self.canConnection.cansend(0x21,[temp])
            time.sleep(0.05)

def main():
    print("main should not be called")



if __name__ == "__main__":
    main()