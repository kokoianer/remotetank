#!/usr/bin/env python3

import socket
import time
import pickle
import threading
import joyStickReader1_0 as joyStickReader
import com2_0 as com
import camera1_0 as cameraserver
import servo2_0 as servo
import controller2_0 as control
from queue import Queue

HEADERSIZE =10
SERVERIP='192.168.4.1'
PORTCOM=12349
PORTVID=5001
panziDict = {"power":1, "soc":99999}
SENDINTERVAL = 0.1 #Hz
TIMEOUT = 2.0 #Seconds


def main():


    F710 = joyStickReader.F710()
    try :
        file=open("/dev/input/jsF710", "rb")
        print("F710 Detected")
        file.close()
        time.sleep(0.1)
        F710.start()
    except:
        print("F710 not present")
        connection = com.SocketCom(SERVERIP, PORTCOM, panziDict, F710.F710, SENDINTERVAL,TIMEOUT, debug = True)
        connection.startServer()


    controller = control.Controller()
    controller.start(F710.F710)

    cameraServo = servo.Servo()
    cameraServo.start(F710.F710)

    
    thread3=threading.Thread(target = cameraserver.app.run(host=SERVERIP, port=PORTVID, threaded=True))
    thread3.daemon = True
    thread3.start()

    thread2=threading.Thread(target = printer, args=[F710.F710, panziDict])
    thread2.daemon = True
    thread2.start()

    print("all threads started")
    while(True):
        time.sleep(0.1) #important, sometimes this uses a lot of CPU
        pass

def printer(pF710, ppanziDict):
    while True:
        #print(f"F710:{pF710}")
        #print(pF710['A'])
        #print(f"panziDict:{ppanziDict}")
        time.sleep(1)



if __name__ == "__main__":
    main()