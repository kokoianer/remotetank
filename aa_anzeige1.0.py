import socket
import time
import pickle
import threading
import joyStickReader1_0 as joyStickReader
import com2_0 as com
from queue import Queue

HEADERSIZE =10
SERVERIP='192.168.4.1'
PORTCOM=12349
panziDict = {"power":7, "soc":10}
SENDINTERVAL = 0.1 #Hz
TIMEOUT = 2.0 #Seconds


def main():
    
    #commuication(F700)
    #(serverIP, serverPort, sendItem, recItem, sendInterval):
    #thread0=threading.Thread(target = F710_reader.readF710, args=[F710_reader.F710])
    #thread0.daemon = True
    #thread0.start()



    F710 = joyStickReader.F710()
    F710.start()

    connection = com.SocketCom(SERVERIP, PORTCOM, F710.F710, panziDict, SENDINTERVAL,TIMEOUT, debug = True)
    connection.startClient()

    thread3=threading.Thread(target = printer, args=[F710.F710, panziDict])
    thread3.daemon = True
    thread3.start()

    print("all threads started")
    while(True):
        time.sleep(1)
        pass

def printer(pF710, ppanziDict):
    while True:
        #print(f"F710:{pF710}")
        #print(f"panziDict:{ppanziDict}")
        time.sleep(1)



if __name__ == "__main__":
    main()