# HW interface is from PIGPIO
# http://abyz.me.uk/rpi/pigpio/
import pigpio
import threading
import time


class Servo: 
    def __init__(self):
        self.pi=pigpio.pi()
        self.enPin = 17
        self.sevoPinRL = 6
        self.sevoPinUD = 5
        self.pi.set_mode(self.enPin,pigpio.OUTPUT)
        self.pi.write(self.enPin,1)

    def start(self,inputDict, debug = False):
        self.inputDict = inputDict
        self.debug = debug
        self.ServoThread = threading.Thread(target = self.run,args=[])
        self.ServoThread.daemon = True
        self.ServoThread.start()

    def run(self):
        print("servo running")
        while(True):
            if(self.inputDict["LJ_UD"]<-1):
                    upDown=0
                    leftRight=0
            else:
                    upDown=-self.inputDict["LJ_UD"]
                    leftRight=self.inputDict["LJ_RL"]
            self.pi.set_servo_pulsewidth(self.sevoPinRL, (1500-leftRight*500)) # 1500 centre
            self.pi.set_servo_pulsewidth(self.sevoPinUD, (1500+upDown*500)) # 1500 centre
            time.sleep(0.05)


def main():
    print("main should not be called")



if __name__ == "__main__":
    main()