
# HW connection is from the following site:
# https://vimtut0r.com/2017/01/17/can-bus-with-raspberry-pi-howtoquickstart-mcp2515-kernel-4-4-x/
#
# modify the /boot/config.txt file like below:
# ...
# # Uncomment some or all of these to enable the optional hardware interfaces
# #dtparam=i2c_arm=on
# #dtparam=i2s=on
# dtparam=spi=on
# dtoverlay=mcp2515-can0,oscillator=8000000,interrupt=25
# dtoverlay=spi1-1cs
# ...
# via rc.local add the auto up of the can:
# sudo ip link set can0 up type can bitrate 500000

import socket
import struct


class CanCom:
    def __init__(self, debug = False):
        self.debug = debug

    def connect(self):
        self.sock = socket.socket(socket.PF_CAN, socket.SOCK_RAW, socket.CAN_RAW)
        self.fmt = "<IB3x8s"
        try:
            self.sock.bind(("can0",))
        except OSError:
            if(self.debug == True):
                sys.stderr.write("Could not bind to interface can0\n")
                print("cannot bind socket")
                # do something about the error.

    def cansend(self,canID, data):
        temp=0
        for x in data:
            temp=temp*256+x
        if(self.debug):
            print("cansend"+str(hex(temp)))
        try:
            self.sock.send(struct.pack(self.fmt, canID,8,temp.to_bytes(8, byteorder = 'big')))
            if(self.debug):
                print("cansend")
        except:
            if(self.debug):
                print("cansend not working")

def main():
    print("main should not be called")



if __name__ == "__main__":
    main()