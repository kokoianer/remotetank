import time
import threading
import struct

class F710:
    def __init__(self):
        self.F710 = {"A":0, "B":0,"X":0,"Y":0,"LB":0,"RB":0,"Back":0,"Start":0,"LJ_B":0, "RJ_B":0,"RJ_RL":0,"RJ_UD":0,"LJ_RL":0,"LJ_UD":0,"LT":0,"RT":0}
        #All analog values are normalized to -1 to 1
        #since (at least my) controller analog stick is sometime misses the 0 point, I set all values smaller than 0.01 to 0
        #All buttons are either 0 or 1
        

    def start(self, debug = False, devicePath = "/dev/input/jsF710"):
        #the decvice path can be fixed with an UDEV rule
        #for the F710 I used the following rule
        # cd /etc/udev/rules.d 
        # sudo leafpad 51-user.rules
        # ACTION=="add",KERNEL=="event?",ATTRS{name}=="Logitech Gamepad F710",SYMLINK+="input/eventF710"

        self.debug = debug
        self.devicePath = devicePath

        self.F710ReaderThread = threading.Thread(target = self.readF710,args=[])
        self.F710ReaderThread.daemon = True
        self.F710ReaderThread.start()


    def readF710(self):
        
        print("thread readF710 started")
        EVENT_SIZE=struct.calcsize("LhBB")
        try:
            file=open(self.devicePath, "rb")
            event=file.read(EVENT_SIZE)
        except:
            print("no Joystick found")
            while True:
                time.sleep(1)
                pass
        
        while event:
            event=file.read(EVENT_SIZE)
            (js_usec,js_value,js_type,js_number)=struct.unpack("LhBB", event)
            if(self.debug):
                print("js_value:",js_value,"  js_type:",js_type,"  js_number:",js_number)
            if(js_type==1):
                if(js_number==0):
                    self.F710["A"]=js_value
                elif(js_number==1):
                    self.F710["B"]=js_value
                elif(js_number==2):
                    self.F710["X"]=js_value
                elif(js_number==3):
                    self.F710["Y"]=js_value
                elif(js_number==4):
                    self.F710["LB"]=js_value
                elif(js_number==5):
                    self.F710["RB"]=js_value
                elif(js_number==6):
                    self.F710["Back"]=js_value
                elif(js_number==7):
                    self.F710["Start"]=js_value
                elif(js_number==9):
                    self.F710["LJ_B"]=js_value
                elif(js_number==10):
                    self.F710["RJ_B"]=js_value
            elif(js_type==2):
                if(js_number==3):
                    self.F710["RJ_RL"]=js_value/32767
                    if(-0.01<self.F710["RJ_RL"]<0.01):
                        self.F710["RJ_RL"]=0
                elif(js_number==4):
                    self.F710["RJ_UD"]=-js_value/32767
                    if(-0.01<self.F710["RJ_UD"]<0.01):
                        self.F710["RJ_UD"]=0
                elif(js_number==0):
                    self.F710["LJ_RL"]=js_value/32767
                    if(-0.01<self.F710["LJ_RL"]<0.01):
                        self.F710["LJ_RL"]=0
                elif(js_number==1):
                    self.F710["LJ_UD"]=-js_value/32767
                    if(-0.01<self.F710["LJ_UD"]<0.01):
                        self.F710["LJ_UD"]=0
                elif(js_number==2):
                    self.F710["LT"]=js_value/32767
                    if(-0.01<self.F710["LT"]<0.01):
                        self.F710["LT"]=0
                elif(js_number==5):
                    self.F710["RT"]=js_value/32767
                    if(-0.01<self.F710["RT"]<0.01):
                        self.F710["RT"]=0


def main():
    print("main should not be called")



if __name__ == "__main__":
    main()